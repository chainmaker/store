/*
Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.

SPDX-License-Identifier: Apache-2.0
*/

package store

import (
	"encoding/json"
	"os"
	"path"
	"testing"

	"chainmaker.org/chainmaker/common/v2/wal"
	"chainmaker.org/chainmaker/protocol/v2"
	"chainmaker.org/chainmaker/protocol/v2/test"
	"chainmaker.org/chainmaker/store/v2/bigfilterdb"
	"chainmaker.org/chainmaker/store/v2/binlog"
	"chainmaker.org/chainmaker/store/v2/blockdb"
	"chainmaker.org/chainmaker/store/v2/conf"
	"chainmaker.org/chainmaker/store/v2/contracteventdb"
	"chainmaker.org/chainmaker/store/v2/historydb"
	"chainmaker.org/chainmaker/store/v2/resultdb"
	"chainmaker.org/chainmaker/store/v2/rolling_window_cache"
	"chainmaker.org/chainmaker/store/v2/statedb"
	"chainmaker.org/chainmaker/store/v2/txexistdb"
)

func genTestConfig() *conf.StorageConfig {
	config := &conf.StorageConfig{}
	cJson := `{
"StorePath":"../data/org1/ledgerData1",
"DbPrefix":"","WriteBufferSize":0,
"BloomFilterBits":0,
"BlockWriteBufferSize":0,
"DisableHistoryDB":false,
"DisableResultDB":false,
"DisableContractEventDB":true,
"LogDBWriteAsync":false,
"BlockDbConfig":{
	"Provider":"leveldb",
	"LevelDbConfig":{
		"store_path":"../data/org1/blocks"
	},
	"BadgerDbConfig":null,
	"SqlDbConfig":null
},
"TxExistDbConfig":{
	"Provider":"leveldb",
	"LevelDbConfig":{
		"store_path":"../data/org1/txexist"
	},
	"BadgerDbConfig":null,
	"SqlDbConfig":null
},
"StateDbConfig":{
	"Provider":"leveldb",
	"LevelDbConfig":{"store_path":"../data/org1/statedb"},
	"BadgerDbConfig":null,
	"SqlDbConfig":null,
	"BloomConfig": {
		"Enable": true,
    	"DumpPath": "./bloom",
    	"KeysCapacity": 200000000,
    	"FalsePositiveRate": 0.01,
    	"DumpPerBlocksCommitted": 10
	}
},
"HistoryDbConfig":{
	"Provider":"leveldb",
	"LevelDbConfig":{"store_path":"../data/org1/history"},
	"BadgerDbConfig":null,"SqlDbConfig":null
},
"ResultDbConfig":{
	"Provider":"leveldb",
	"LevelDbConfig":{"store_path":"../data/org1/result"},
	"BadgerDbConfig":null,
	"SqlDbConfig":null
},
"ContractEventDbConfig":{
	"Provider":"sql",
	"LevelDbConfig":null,
	"BadgerDbConfig":null,
	"SqlDbConfig":{
		"dsn":"root:password@tcp(127.0.0.1:3306)/",
		"sqldb_type":"mysql"
	}
},
"UnArchiveBlockHeight":0,
"Encryptor":"sm4",
"EncryptKey":"1234567890123456"
}`
	//config.DisableBigFilter = true
	if err := json.Unmarshal([]byte(cJson), config); err != nil {
		panic(err)
	}
	return config
}

func Test_storeBuilder_create(t *testing.T) {
	builder := newStoreBuilder("chain1", genTestConfig(), test.GoLogger{}, nil)
	var (
		binLogger       binlog.BinLogger
		blockDB         blockdb.BlockDB
		stateDB         statedb.StateDB
		historyDB       historydb.HistoryDB
		contractEventDB contracteventdb.ContractEventDB
		resultDB        resultdb.ResultDB
		txExistDB       txexistdb.TxExistDB
		commonDB        protocol.DBHandle
		walLog          *wal.Log
		bigFilterDB     bigfilterdb.BigFilterDB
		rwCache         rolling_window_cache.RollingWindowCache
	)
	type args struct {
		out interface{}
	}
	tests := []struct {
		name    string
		fields  *storeBuilder
		args    args
		wantErr bool
	}{
		{
			name:    "create_binLogger",
			fields:  builder,
			args:    args{out: &binLogger},
			wantErr: false,
		},
		{
			name:    "create_blockdb",
			fields:  builder,
			args:    args{out: &blockDB},
			wantErr: false,
		},
		{
			name:    "create_stateDB",
			fields:  builder,
			args:    args{out: &stateDB},
			wantErr: false,
		},
		{
			name:    "create_historyDB",
			fields:  builder,
			args:    args{out: &historyDB},
			wantErr: false,
		},
		{
			name:    "create_contractEventDB",
			fields:  builder,
			args:    args{out: &contractEventDB},
			wantErr: false,
		},
		{
			name:    "create_resultDB",
			fields:  builder,
			args:    args{out: &resultDB},
			wantErr: false,
		},
		{
			name:    "create_txExistDB",
			fields:  builder,
			args:    args{out: &txExistDB},
			wantErr: false,
		},
		{
			name:    "create_commonDB",
			fields:  builder,
			args:    args{out: &commonDB},
			wantErr: false,
		},
		{
			name:    "create_wal",
			fields:  builder,
			args:    args{out: &walLog},
			wantErr: false,
		},
		{
			name:    "create_bigFilterDB",
			fields:  builder,
			args:    args{out: &bigFilterDB},
			wantErr: false,
		},
		{
			name:    "create_rwCache",
			fields:  builder,
			args:    args{out: &rwCache},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			s := tt.fields
			if err := s.create(tt.args.out); (err != nil) != tt.wantErr {
				t.Errorf("storeBuilder.create() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

func Test_storeBuilder_Build(t *testing.T) {
	dbConfig := genTestConfig()
	chainId := "chain_test"
	builder := newStoreBuilder(chainId, dbConfig, test.GoLogger{}, nil)
	defer func() {
		if dbConfig.StateDbConfig.EnableBloom() {
			os.RemoveAll(path.Join(dbConfig.StateDbConfig.BloomConfig.DumpPath, chainId))
		}
	}()
	tests := []struct {
		name    string
		fields  *storeBuilder
		want    protocol.BlockchainStore
		wantErr bool
	}{
		{
			name:   "store_build",
			fields: builder,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			b := tt.fields
			got, err := b.Build()
			if (err != nil) != tt.wantErr {
				t.Errorf("storeBuilder.Build() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			t.Log(got.GetLastHeight())
			t.Log(got.GetLastBlock())
		})
	}
}
