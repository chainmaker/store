module chainmaker.org/chainmaker/store/performance

go 1.15

require (
	chainmaker.org/chainmaker/localconf/v2 v2.1.1-0.20211214124610-bb7620382194
	chainmaker.org/chainmaker/logger/v2 v2.1.1-0.20211214124250-621f11b35ab0
	chainmaker.org/chainmaker/pb-go/v2 v2.3.5-0.20240423085802-17ae02ada0d6
	chainmaker.org/chainmaker/protocol/v2 v2.3.5-0.20240422081833-9e2c922d11db
	chainmaker.org/chainmaker/store/v2 v2.1.1
	chainmaker.org/chainmaker/utils/v2 v2.3.5-0.20240429082210-d1cbb23924b4
	github.com/spf13/cobra v1.1.1
	github.com/studyzy/sqlparse v0.0.0-20210520090832-d40c792e1576 // indirect
)

replace (
	chainmaker.org/chainmaker/store/v2 => ../
	google.golang.org/grpc => google.golang.org/grpc v1.26.0
)
